CREATE USER 'owncloud'@'%' IDENTIFIED BY 'oc_owncloud';
CREATE DATABASE IF NOT EXISTS owncloud_db;
GRANT ALL ON owncloud_db.* TO 'owncloud'@'%' IDENTIFIED BY 'oc_owncloud';