
# Recreacion de entorno de docker

Para acceder a los servicios, se hace:

```
<nombre de servicio>.docker-enviroment-tristan.cyberia.localhost
```
Servicios actuales disponibles:

+ adminer
+ owncloud
+ wako
+ cyberia
+ adminer
+ development (nginx)

-------------------------------------------

## Servicio de Desarrollo servido por Nginx


Si se intenta acceder al servicio de development, no existira ningun proyecto dentro la carpeta code asique es necesario inicializarlo con el comando

```bash
sudo docker run -ti --rm -u $(id -u):$(id -g) -v $PWD:/var/www/html composer bash
```
y usar los comandos de composer para descargar laravel.


Luego es necesario modificar el docker-compose para que agregue el proyecto como un volumen, dentro del contenedor de nginx

```Docker
development:
    image: nginx
    volumes:
      - ./code/blog:/code # <-  Este de aqui es el que se debe modificar
      # - ./code/<NombreDelProyecto>:/code <-- Ejemplo
    labels:
      - "traefik.basic.port=4200"

```